package com.finalproject.paymentGatewayApi.Enums;

public enum OperationType {

    DEPOSIT,
    WITHDRAWAL
}
