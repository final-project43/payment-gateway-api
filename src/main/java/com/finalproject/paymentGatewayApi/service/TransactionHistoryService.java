package com.finalproject.paymentGatewayApi.service;

import com.finalproject.paymentGatewayApi.Enums.OperationType;
import com.finalproject.paymentGatewayApi.repository.Interfaces.TransactionHistoryRepository;
import com.finalproject.paymentGatewayApi.repository.models.Beneficiary;
import com.finalproject.paymentGatewayApi.repository.models.TransactionHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TransactionHistoryService {

    private final TransactionHistoryRepository transactionHistoryRepository;

    @Autowired
    public TransactionHistoryService(TransactionHistoryRepository transactionHistoryRepository) {
        this.transactionHistoryRepository = transactionHistoryRepository;
    }

    public TransactionHistory saveTransaction(Beneficiary beneficiary, BigDecimal amountUsd, BigDecimal localAmount, OperationType operationType) {
        TransactionHistory transactionHistory = new TransactionHistory();
        transactionHistory.setIdBeneficiary(beneficiary.getIdBeneficiary());
        transactionHistory.setLocalAmount(localAmount);
        transactionHistory.setAmount(amountUsd);
        transactionHistory.setOperationType(operationType.name());
        log.info("Saving transaction");
        return transactionHistoryRepository.save(transactionHistory);
    }
}
