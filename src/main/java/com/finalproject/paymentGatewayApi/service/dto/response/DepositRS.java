package com.finalproject.paymentGatewayApi.service.dto.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DepositRS {

   private String transactionId;

   private BigDecimal depositAmount;

   private BigDecimal currentBalance;

   private String currency;

   private String error;

}
