package com.finalproject.paymentGatewayApi.service.dto.request;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class TransactionRQ {

   private BigDecimal amount;

}
