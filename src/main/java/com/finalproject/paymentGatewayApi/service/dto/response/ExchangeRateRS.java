package com.finalproject.paymentGatewayApi.service.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExchangeRateRS {

    private String currency;

    private BigDecimal amount;

    private String error;

}
