package com.finalproject.paymentGatewayApi.service.dto.request;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class FeeApiRQ {

    private Integer clientId;

    private BigDecimal amountUsd;

}
