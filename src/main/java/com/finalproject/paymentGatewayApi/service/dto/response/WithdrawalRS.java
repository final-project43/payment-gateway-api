package com.finalproject.paymentGatewayApi.service.dto.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WithdrawalRS {

   private String transactionId;

   private String currency;

   private BigDecimal currentBalance;

   private BigDecimal withdrawalAmount;

   private BigDecimal fee;

   private String error;

}
