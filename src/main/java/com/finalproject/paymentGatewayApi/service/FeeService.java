package com.finalproject.paymentGatewayApi.service;

import com.finalproject.paymentGatewayApi.repository.models.Beneficiary;
import com.finalproject.paymentGatewayApi.service.dto.request.FeeApiRQ;
import com.finalproject.paymentGatewayApi.service.dto.response.FeeApiRS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.net.URI;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FeeService {

    @Value("${external.url.fee-api}")
    private String feeUrl;

    private final WebClient webClient;

    @Autowired
    public FeeService(WebClient webClient) {
        this.webClient = webClient;
    }

    public FeeApiRS getFeeRS(Beneficiary beneficiary, BigDecimal amount) {
       log.info("Calculating fee for " + amount);
        return callFeeApi(beneficiary.getIdBeneficiary(), amount);
    }

    private FeeApiRS callFeeApi(Integer clientId, BigDecimal amount) {
        FeeApiRQ feeApiRQ = new FeeApiRQ();
        feeApiRQ.setClientId(clientId);
        feeApiRQ.setAmountUsd(amount);

        URI uri = UriComponentsBuilder.fromUriString(feeUrl)
                .path("/feeCalculator")
                .build().toUri();

       return webClient
               .post()
               .uri(uri)
               .bodyValue(feeApiRQ)
               .retrieve()
               .bodyToMono(FeeApiRS.class)
               .block();
    }
}
