package com.finalproject.paymentGatewayApi.service;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.finalproject.paymentGatewayApi.Enums.OperationType;
import com.finalproject.paymentGatewayApi.repository.Interfaces.BeneficiaryRepository;
import com.finalproject.paymentGatewayApi.repository.Interfaces.ClientBalanceRepository;
import com.finalproject.paymentGatewayApi.repository.models.Beneficiary;
import com.finalproject.paymentGatewayApi.repository.models.ClientBalance;
import com.finalproject.paymentGatewayApi.repository.models.TransactionHistory;
import com.finalproject.paymentGatewayApi.service.dto.request.TransactionRQ;
import com.finalproject.paymentGatewayApi.service.dto.response.BalanceRS;
import com.finalproject.paymentGatewayApi.service.dto.response.DepositRS;
import com.finalproject.paymentGatewayApi.service.dto.response.FeeApiRS;
import com.finalproject.paymentGatewayApi.service.dto.response.WithdrawalRS;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PaymentService {

   private final ClientBalanceRepository clientBalanceRepository;

   private final BeneficiaryRepository beneficiaryRepository;

   private final TransactionHistoryService transactionHistoryService;

   private final ExchangeRateService exchangeRateService;

   private final FeeService feeService;

   @Autowired
   public PaymentService(ClientBalanceRepository clientBalanceRepository, BeneficiaryRepository beneficiaryRepository,
         TransactionHistoryService transactionHistoryService, ExchangeRateService exchangeRateService, FeeService feeService) {
      this.beneficiaryRepository = beneficiaryRepository;
      this.transactionHistoryService = transactionHistoryService;
      this.clientBalanceRepository = clientBalanceRepository;
      this.exchangeRateService = exchangeRateService;
      this.feeService = feeService;
   }

   public ResponseEntity<BalanceRS> getBalance(Integer clientId) {
      Optional<ClientBalance> optionalClientBalance = clientBalanceRepository.findByIdBeneficiary(clientId);
      BalanceRS balanceRS = new BalanceRS();
      if (optionalClientBalance.isEmpty()) {
         balanceRS.setError("Client " + clientId + " not found.");
         return new ResponseEntity<>(balanceRS, HttpStatus.NOT_FOUND);
      } else {
         log.info("Getting balance of client " + clientId);
         ClientBalance clientBalance = optionalClientBalance.get();
         BigDecimal localAvailableBalance = exchangeRateService.exchangeAmount(clientBalance.getBeneficiary(), clientBalance.getAmount(), true);
         balanceRS.setCurrency(clientBalance.getBeneficiary().getCountry().getCurrency());
         balanceRS.setAvailableBalance(localAvailableBalance);
         return ResponseEntity.ok(balanceRS);
      }
   }

   public ResponseEntity<DepositRS> makeDeposit(Integer clientId, TransactionRQ transactionRQ) {
      if (transactionRQ.getAmount().compareTo(BigDecimal.ZERO) > 0) {
         Optional<ClientBalance> clientBalanceOptional = clientBalanceRepository.findByIdBeneficiary(clientId);
         if (clientBalanceOptional.isPresent()) {
            Beneficiary beneficiary = clientBalanceOptional.get().getBeneficiary();
            boolean isUsd = beneficiary.getCountry().getCurrency().equals("USD");

            BigDecimal usdAmount = exchangeRateService.exchangeAmount(beneficiary, transactionRQ.getAmount(), isUsd);

            ClientBalance clientBalance = clientBalanceOptional.get();
            BigDecimal actualAmount = clientBalance.getAmount().add(usdAmount);
            clientBalance.setAmount(actualAmount);
            clientBalanceRepository.save(clientBalance);

            log.info("Depositing $" + usdAmount + "in client " + clientId);

            BigDecimal currencyLocalBalance = exchangeRateService.exchangeAmount(beneficiary, actualAmount, true);
            String transactionId = UUID.randomUUID().toString();

            DepositRS depositRS = new DepositRS();
            depositRS.setDepositAmount(transactionRQ.getAmount());
            depositRS.setCurrentBalance(currencyLocalBalance);
            depositRS.setTransactionId(transactionId);
            depositRS.setCurrency(clientBalance.getBeneficiary().getCountry().getCurrency());

            updateScore(beneficiary);
            transactionHistoryService.saveTransaction(beneficiary, usdAmount, transactionRQ.getAmount(), OperationType.DEPOSIT);
            return ResponseEntity.ok(depositRS);
         }
      } else {
         return errorDeposit("Can´t deposit " + transactionRQ.getAmount(), HttpStatus.NOT_ACCEPTABLE);
      }
      return errorDeposit("Client " + clientId + " not found", HttpStatus.NOT_FOUND);
   }

   public ResponseEntity<WithdrawalRS> withdrawal(Integer clientId, BigDecimal amount) {
      Optional<ClientBalance> optionalClientBalance = clientBalanceRepository.findByIdBeneficiary(clientId);
      if (optionalClientBalance.isEmpty()) {
         return errorWithdrawal("Client " + clientId + " not found", HttpStatus.NOT_FOUND);
      } else {
         ClientBalance clientBalance = optionalClientBalance.get();
         boolean isUsd = clientBalance.getBeneficiary().getCountry().getCurrency().equals("USD");
         updateScore(clientBalance.getBeneficiary());
         return createWithdrawalRS(clientBalance, amount, isUsd);
      }
   }

   private ResponseEntity<DepositRS> errorDeposit(String error, HttpStatus httpStatus) {
      DepositRS depositResponse = new DepositRS();
      depositResponse.setError(error);
      return new ResponseEntity<>(depositResponse, httpStatus);
   }

   private ResponseEntity<WithdrawalRS> errorWithdrawal(String error, HttpStatus httpStatus) {
      WithdrawalRS withdrawalRS = new WithdrawalRS();
      withdrawalRS.setError(error);
      return new ResponseEntity<>(withdrawalRS, httpStatus);
   }

   private ResponseEntity<WithdrawalRS> createWithdrawalRS(ClientBalance clientBalance, BigDecimal amount, boolean isUsd) {
      Beneficiary beneficiary = clientBalance.getBeneficiary();
      BigDecimal localAmount = amount;
      amount = exchangeRateService.exchangeAmount(beneficiary, amount, isUsd);
      if (clientBalance.getAmount().compareTo(amount) > 0) {
         if (amount.compareTo(BigDecimal.ZERO) > 0) {

            FeeApiRS fee = feeService.getFeeRS(beneficiary, amount);

            clientBalance.setBeneficiary(beneficiary);
            BigDecimal usdBalance = clientBalance.getAmount().subtract(amount);
            log.info("Withdrawing $" + amount + "of client " + beneficiary.getIdBeneficiary() + " balance.");
            clientBalance.setAmount(usdBalance);
            clientBalanceRepository.save(clientBalance);

            BigDecimal withdrawalAmountLocalCurrency = exchangeRateService.exchangeAmount(beneficiary, fee.getFinalAmount(), true);
            BigDecimal feeLocalCurrency = exchangeRateService.exchangeAmount(beneficiary, fee.getFee(), true);
            BigDecimal currencyLocalBalance = exchangeRateService.exchangeAmount(beneficiary, usdBalance, true);

            WithdrawalRS withdrawalRS = new WithdrawalRS();
            withdrawalRS.setCurrency(isUsd ? "USD" : beneficiary.getCountry().getCurrency());
            withdrawalRS.setCurrentBalance(currencyLocalBalance);
            withdrawalRS.setWithdrawalAmount(withdrawalAmountLocalCurrency);
            withdrawalRS.setFee(feeLocalCurrency); //

            TransactionHistory transactionHistory = transactionHistoryService.saveTransaction(beneficiary, amount, localAmount,
                  OperationType.WITHDRAWAL);
            withdrawalRS.setTransactionId(transactionHistory.getIdTransaction());
            return ResponseEntity.ok(withdrawalRS);
         } else {
            return errorWithdrawal("Can´t withdrawal this amount!", HttpStatus.NOT_ACCEPTABLE);
         }
      } else {
         return errorWithdrawal("Insufficient funds!", HttpStatus.FORBIDDEN);
      }
   }

   private void updateScore(Beneficiary beneficiary) {
      beneficiary.setScore(beneficiary.getScore() + 1);
      beneficiaryRepository.save(beneficiary);
   }

}
