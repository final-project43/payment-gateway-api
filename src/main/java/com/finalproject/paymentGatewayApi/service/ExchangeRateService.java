package com.finalproject.paymentGatewayApi.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import com.finalproject.paymentGatewayApi.repository.models.Beneficiary;
import com.finalproject.paymentGatewayApi.service.dto.response.ExchangeRateRS;

@Service
public class ExchangeRateService {

   @Value("${external.url.exchange-rate-api}")
   private String exchangeRateUrl;

   private final WebClient webClient;

   @Autowired
   public ExchangeRateService(WebClient webClient) {
      this.webClient = webClient;
   }

   public BigDecimal exchangeAmount(Beneficiary beneficiary, BigDecimal amount, boolean isUsd) {
      if (isUsd) {
         return getExchangeRS("USD", beneficiary.getCountry().getCurrency(), amount).setScale(2, RoundingMode.DOWN);
      } else {
         return getExchangeRS(beneficiary.getCountry().getCurrency(), "USD", amount).setScale(2, RoundingMode.DOWN);
      }
   }

   private BigDecimal getExchangeRS(String currencyFrom, String currencyTo, BigDecimal amount) {

      URI uri = UriComponentsBuilder
            .fromUriString(exchangeRateUrl)
            .path("/exchangeRate")
            .queryParam("from", currencyFrom)
            .queryParam("to", currencyTo)
            .queryParam("amount", amount)
            .build()
            .toUri();

      ExchangeRateRS rs = webClient.get().uri(uri).retrieve().bodyToMono(ExchangeRateRS.class).block();

      return Objects.requireNonNull(rs).getAmount();
   }

}
