package com.finalproject.paymentGatewayApi.repository.Interfaces;

import com.finalproject.paymentGatewayApi.repository.models.Beneficiary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BeneficiaryRepository extends JpaRepository<Beneficiary, Integer> {

    Optional<Beneficiary> findByIdBeneficiary(Integer id);

}
