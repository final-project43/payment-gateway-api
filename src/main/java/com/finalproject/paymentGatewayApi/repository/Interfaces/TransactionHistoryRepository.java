package com.finalproject.paymentGatewayApi.repository.Interfaces;

import com.finalproject.paymentGatewayApi.repository.models.TransactionHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, Integer> {
}
