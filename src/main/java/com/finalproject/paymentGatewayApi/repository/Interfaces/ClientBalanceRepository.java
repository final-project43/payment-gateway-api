package com.finalproject.paymentGatewayApi.repository.Interfaces;

import com.finalproject.paymentGatewayApi.repository.models.ClientBalance;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientBalanceRepository extends JpaRepository<ClientBalance, Integer> {

    @EntityGraph(attributePaths = {"beneficiary", "beneficiary.country"})
    Optional<ClientBalance> findByIdBeneficiary(Integer idBeneficiary);
}
