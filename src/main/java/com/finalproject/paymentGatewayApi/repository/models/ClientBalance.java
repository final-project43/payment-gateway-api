package com.finalproject.paymentGatewayApi.repository.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@Table(name = "client_balance")
public class ClientBalance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_balance")
    private Integer id;

    @Column(name = "id_beneficiary")
    private Integer idBeneficiary;

    @Column(name = "amount")
    private BigDecimal amount;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_beneficiary", insertable = false, updatable = false)
    private Beneficiary beneficiary;
}
