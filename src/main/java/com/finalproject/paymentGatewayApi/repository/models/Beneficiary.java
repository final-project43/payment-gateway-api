package com.finalproject.paymentGatewayApi.repository.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "beneficiary")
public class Beneficiary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_beneficiary")
    private Integer idBeneficiary;

    @Column(name = "name")
    private String name;

    @Column(name = "document")
    private Integer document;

    @Column(name = "document_type")
    private String documentType;

    @Column(name = "score")
    private Integer score;

    @Column(name = "id_country")
    private Integer idCountry;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_country", insertable = false, updatable = false)
    private Country country;

}
