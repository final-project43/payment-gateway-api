package com.finalproject.paymentGatewayApi.repository.models;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "transaction_history")

public class TransactionHistory {

   @Id
   @Column(name = "id_transaction")
   private String idTransaction = UUID.randomUUID().toString();

   @Column(name = "id_beneficiary")
   private Integer idBeneficiary;

   @Column(name = "amount")
   private BigDecimal amount;

   @Column(name = "local_amount")
   private BigDecimal localAmount;

   @Column(name = "operation_type")
   private String operationType;

}
