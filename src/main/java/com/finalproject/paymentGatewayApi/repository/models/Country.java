package com.finalproject.paymentGatewayApi.repository.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "country")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_country")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "iso")
    private String iso;

    @Column(name = "currency")
    private String currency;

}
