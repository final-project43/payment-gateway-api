package com.finalproject.paymentGatewayApi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.finalproject.paymentGatewayApi.service.PaymentService;
import com.finalproject.paymentGatewayApi.service.dto.request.TransactionRQ;
import com.finalproject.paymentGatewayApi.service.dto.response.BalanceRS;
import com.finalproject.paymentGatewayApi.service.dto.response.DepositRS;
import com.finalproject.paymentGatewayApi.service.dto.response.WithdrawalRS;

@RestController
@RequestMapping
public class PaymentController {

   private final PaymentService paymentService;

   @Autowired
   public PaymentController(PaymentService paymentService) {
      this.paymentService = paymentService;
   }

   @PostMapping("/client/{clientId}/deposit")
   public ResponseEntity<DepositRS> deposit(@PathVariable Integer clientId, @RequestBody TransactionRQ transactionRQ) {
      return paymentService.makeDeposit(clientId, transactionRQ);
   }

   @PostMapping("/client/{clientId}/withdrawal")
   public ResponseEntity<WithdrawalRS> withdrawing(@PathVariable Integer clientId, @RequestBody TransactionRQ transactionRQ) {
      return paymentService.withdrawal(clientId, transactionRQ.getAmount());
   }

   @GetMapping("/client/{clientId}/balance")
   public ResponseEntity<BalanceRS> getBalance(@PathVariable Integer clientId) {
      return paymentService.getBalance(clientId);
   }

}
