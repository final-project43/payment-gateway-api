# Payment Gateway API

Api encargada de orquestar y manejar la lógica de negocio principal de depositos y retiros de dinero.

# Endpoints

## Flujo de depósito
1. El cliente ejecuta una solicitud de depósito de dinero.
2. El sistema convierte el monto expresado en moneda local hacia USD.
3. El sistema guarda el monto en USD al balance del cliente.
4. El sistema registra la transacción en la tabla transaction_history.
5. El sistema responde que se ha hecho el depósito informando el balance actual en moneda local del cliente, monto del depósito y el ID de transacción. Si el cliente no existe deberá informar con el código HTTP y mensaje correspondiente.


Sirve para ejecutar una solicitud de depósito de dinero.

`[POST] client/{clientId}/deposit`

### Path variable

- **clientId**: Es el id del cliente que requiere depositar dinero.

### Body

```json
{
  "amount": 1234.5
}
```

### Response:

code : 200

```json
{
  "transaction_id": "123asdasd21342",
  "depositAmount": 1000,
  "currentBalance": 5000
}
```

code : 404

```json
{
  "error": "Client {clientId} not found"
}
```

## Flujo de retiro de fondos

1. El cliente solicita retirar un monto específico en su moneda local.
2. El sistema verifica que el cliente exista.
3. El sistema verifica que existan fondos suficientes. Para esto debe convertir el monto en moneda local a USD y comparar con el balance.
4. El sistema consulta el fee de la transacción a Fee Api y este nos retorna el monto en USD menos la comisión.
5. El sistema registra la transacción en la tabla transaction_history.
6. El sistema responde al cliente el fee, el balance actual, el monto retirado y la moneda local (ISO).


Sirve para retirar fondos de un cliente. El valor siempre está expresado en su moneda local.

`[POST] client/{clientId}/withdrawal`

### Path variable

- **clientId**: Es el id del cliente que requiere depositar dinero.

### Body

```json
{
  "amount": 400
}
```
### Response:

code : 200

```json
{
  "transaction_id": "123423453345",
  "currency": "ARS",
  "currentBalance": 1000,
  "withdrawnAmount": 500,
  "fee": 50
}
```

code : 403

```json
{
  "error": "Insufficient funds"
}
```

code : 404

```json
{
  "error": "Client {clientId} not found."
}
```

Endpoint auxiliar que sirve para consultar el balance de un cliente.

`[GET] /client/{clientId}/balance`

### Path variable

- **clientId**: Es el id del cliente que requiere consultar el balance.

### Response:

code : 200

```json
{
  "currency": "ARS",
  "availableBalance": 12354.50
}
```

code : 404

```json
{
  "error": "Client {clientId} not found."
}
```